## Simple status displaying demo that published std_msgs/Strings messages
## to the 'status_talker/status' topic

import rospy
from std_msgs.msg import String
global counter

def talker():
    pub = rospy.Publisher('status_talker/status', String, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(2) # 2hz
    counter = 0
    while not rospy.is_shutdown():
     
        status_str = "status:ok %s" % rospy.get_time()
        warning_str = "status:warning %s" % rospy.get_time()
        error_str = "status:error %s" % rospy.get_time()
        
        
        if counter % 3 == 0:

            rospy.logdebug(warning_str)
            pub.publish(warning_str)
            
        elif counter % 5 == 0 and counter % 3 != 0:

            rospy.logerr(error_str)
            pub.publish(error_str)
            
        else:
            rospy.loginfo(status_str)
            pub.publish(status_str)     
        
               
        counter +=1    
        rate.sleep()

if __name__ == '__main__':

    try:
        
        talker()
    except rospy.ROSInterruptException:
        pass

